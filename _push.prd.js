const RPA_URL_PUSH = "aHR0cHM6Ly9mcC45OWxpdmUuYXNpYQ==";

// This function is checking Browser Type & OS name
function checkBrowserType() {
  let sUsrAg = navigator.userAgent;
  let OSName = "Unknown OS";
  let browserType = "not iPhone browser";
  if (navigator.userAgent.indexOf("Mac") != -1) OSName = "MacOS";
  if (sUsrAg.indexOf("iPhone") != -1) browserType = "iPhone browser";

  return { OSName, browserType };
}

/**
 * This is a function checking user Device is Mobile or not?
 * @return Boolean value.
 */
function checkIsMobile() {
  const SCREEN_WIDTH = 600;
  if (screen.width < SCREEN_WIDTH) {
    return true;
  } else {
    return false;
  }
}

function webpush_closeModal() {
  let modalBg = document.getElementById("rpa-modal-bg");
  let modalContainer = document.getElementById("rpa-modal-container");

  modalBg.classList.contains("hidden") ? null : modalBg.classList.add("hidden");

  modalContainer.classList.contains("hidden") ? null : modalContainer.classList.add("hidden");
}

function webpush_modalContent(data, tag, scenario_ids) {
  return `<div class="modal__push-wrapper-header">
                <span class="modal__push-wrapper-header-title" style="color:black !important;">${data.notify_text}</span>
                <span class="modal__push-wrapper-header-close" onclick="webpush_closeModal();trace_click('${scenario_ids}',-1,'${tag}');">
                &times;
                </span>
            </div>
            <div class="modal__push-wrapper-body">
                <div class="modal__push-wrapper-body-image ${data.imgDirection}">
                    <img src="${data.imgLink}" alt="${data.imgAlt}"/>
                </div>
                <span class="modal__push-wrapper-body-title" style="color:black !important;">${data.title}</span>
                <span class="modal__push-wrapper-body-advertising__copy" style="color:black !important;">${data.advertising_copy}</span>
            </div>
            <div class="modal__push-wrapper-footer">
                <a href="${data.website_access}" target="_blank" style="color:white !important;" class="modal__push-wrapper-footer-acess" onclick="trace_click('${data.imgId}',1,'${tag}');webpush_closeModal();">${data.goto_text}</a>
                <button class="modal__push-wrapper-footer-close" style="color:white !important;" onclick="webpush_closeModal();trace_click('${data.close_imgId}',1,'${tag}');">${data.close_text}</button>
            </div>`;
}

function webpush_openModal(data, position = "center", tag, scenario_ids) {
  let modalContainerContent = webpush_modalContent(data, tag, scenario_ids);

  let modalBg = document.getElementById("rpa-modal-bg");
  let modalContainer = document.getElementById("rpa-modal-container");
  position == "center" ? modalContainer.classList.add("center") : modalContainer.classList.add("bottom__right");

  modalContainer.innerHTML = modalContainerContent;

  modalBg.classList.contains("hidden") ? modalBg.classList.remove("hidden") : null;

  modalContainer.classList.contains("hidden") ? modalContainer.classList.remove("hidden") : null;
}

//----------經典初始化----------
function webpush_modalOnInit() {
  let modalLabelBg = document.createElement("div");
  let modalDivContainer = document.createElement("div");

  modalLabelBg.className = "modal__push-bg hidden";
  modalLabelBg.setAttribute("id", "rpa-modal-bg");

  modalDivContainer.className = "modal__push-wrapper hidden";
  modalDivContainer.setAttribute("id", "rpa-modal-container");

  modalLabelBg.appendChild(modalDivContainer);

  document.body.appendChild(modalLabelBg);
}

//----------輪播右邊----------
function rpaModalRightInit(imageObj, tag, scenario_ids) {
  let rpaModalContainer = document.createElement("div");
  let rpaModalClose = document.createElement("div");
  let rpaModalDotsContainer = document.createElement("div");

  rpaModalContainer.className = "rpa-modal-slider-container rpa-modal-bottom-right";
  rpaModalContainer.style.zIndex = "9999";
  rpaModalDotsContainer.className = "rpa-modal-images-dot";
  rpaModalClose.className = "rpa-modal-slider-close";
  rpaModalClose.innerHTML = `
  <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M2.68306 3.56694C2.43898 3.32286 2.43898 2.92714 2.68306 2.68306C2.92714 2.43898 3.32287 2.43898 3.56694 2.68306L10 9.11612L16.4331 2.68306C16.6771 2.43898 17.0729 2.43898 17.3169 2.68306C17.561 2.92714 17.561 3.32286 17.3169 3.56694L10.8839 10L17.3169 16.4331C17.561 16.6771 17.561 17.0729 17.3169 17.3169C17.0729 17.561 16.6771 17.561 16.4331 17.3169L10 10.8839L3.56694 17.3169C3.32287 17.561 2.92714 17.561 2.68306 17.3169C2.43898 17.0729 2.43898 16.6771 2.68306 16.4331L9.11612 10L2.68306 3.56694Z" fill="white" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
  </svg>`;

  // Cloas icon
  rpaModalClose.onclick = function (event) {
    // event.preventDefault();
    trace_click(scenario_ids, -1, tag);
    document.body.style.position = "";
    let rpaModal = document.querySelectorAll(".rpa-modal-bottom-right");
    rpaModal[0].remove();
  };
  rpaModalContainer.appendChild(rpaModalClose);

  imageObj.forEach((item) => {
    let rpaModalWrapper = document.createElement("div");
    rpaModalWrapper.className = "rpa-modal-slides-wrapper";

    let rpaModalHyperlink = document.createElement("a");
    rpaModalHyperlink.href = item.hyperlink;
    rpaModalHyperlink.setAttribute("target", "_blank");
    rpaModalHyperlink.onclick = function (event) {
      trace_click(item.imgId, 1, tag);
      let rpaModal = document.querySelectorAll(".rpa-modal-bottom-right");
      rpaModal[0].remove();
    };

    let rpaModalImage = document.createElement("img");
    rpaModalImage.src = item.imgLink;
    rpaModalImage.alt = item.imgAlt;

    let rpaModalDotSpan = document.createElement("span");
    rpaModalDotSpan.className = "rpa-modal-image-dot";

    rpaModalHyperlink.appendChild(rpaModalImage);
    rpaModalWrapper.appendChild(rpaModalHyperlink);
    rpaModalContainer.appendChild(rpaModalWrapper);

    rpaModalDotsContainer.appendChild(rpaModalDotSpan);
  });

  rpaModalContainer.appendChild(rpaModalDotsContainer);

  document.body.style.position = "relative";
  document.body.appendChild(rpaModalContainer);

  let index = 0;
  let slides = document.querySelectorAll(".rpa-modal-bottom-right .rpa-modal-slides-wrapper");
  let dot = document.querySelectorAll(".rpa-modal-bottom-right .rpa-modal-image-dot");

  function changeSlide() {
    if (index < 0) {
      index = slides.length - 1;
    }

    if (index > slides.length - 1) {
      index = 0;
    }

    for (let i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
      dot[i].classList.remove("rpa-modal-image-dot-active");
    }

    slides[index].style.display = "block";
    dot[index].classList.add("rpa-modal-image-dot-active");

    index++;

    setTimeout(changeSlide, 5000);
  }

  changeSlide();
}

//----------輪播左邊----------
function rpaModalLeftInit(imageObj, tag, scenario_ids) {
  let rpaModalContainer = document.createElement("div");
  let rpaModalClose = document.createElement("div");
  let rpaModalDotsContainer = document.createElement("div");

  rpaModalContainer.className = "rpa-modal-slider-container rpa-modal-bottom-left";
  rpaModalContainer.style.zIndex = "99999";
  rpaModalDotsContainer.className = "rpa-modal-images-dot";
  rpaModalClose.className = "rpa-modal-slider-close";
  rpaModalClose.innerHTML = `
  <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M2.68306 3.56694C2.43898 3.32286 2.43898 2.92714 2.68306 2.68306C2.92714 2.43898 3.32287 2.43898 3.56694 2.68306L10 9.11612L16.4331 2.68306C16.6771 2.43898 17.0729 2.43898 17.3169 2.68306C17.561 2.92714 17.561 3.32286 17.3169 3.56694L10.8839 10L17.3169 16.4331C17.561 16.6771 17.561 17.0729 17.3169 17.3169C17.0729 17.561 16.6771 17.561 16.4331 17.3169L10 10.8839L3.56694 17.3169C3.32287 17.561 2.92714 17.561 2.68306 17.3169C2.43898 17.0729 2.43898 16.6771 2.68306 16.4331L9.11612 10L2.68306 3.56694Z" fill="white" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
  </svg>`;

  // Cloas icon
  rpaModalClose.onclick = function (event) {
    trace_click(scenario_ids, -1, tag);
    document.body.style.position = "";
    let rpaAds = document.querySelectorAll(".rpa-modal-bottom-left");
    rpaAds[0].remove();
  };

  rpaModalContainer.appendChild(rpaModalClose);

  imageObj.forEach((item) => {
    let rpaModalWrapper = document.createElement("div");
    rpaModalWrapper.className = "rpa-modal-slides-wrapper";

    let rpaModalHyperlink = document.createElement("a");
    rpaModalHyperlink.href = item.hyperlink;
    rpaModalHyperlink.setAttribute("target", "_blank");
    rpaModalHyperlink.onclick = function (event) {
      trace_click(item.imgId, 1, tag);
      let rpaAds = document.querySelectorAll(".rpa-modal-bottom-left");
      rpaAds[0].remove();
    };

    let rpaModalImage = document.createElement("img");
    rpaModalImage.src = item.imgLink;
    rpaModalImage.alt = item.imgAlt;

    let rpaModalDotSpan = document.createElement("span");
    rpaModalDotSpan.className = "rpa-modal-image-dot";

    rpaModalHyperlink.appendChild(rpaModalImage);
    rpaModalWrapper.appendChild(rpaModalHyperlink);
    rpaModalContainer.appendChild(rpaModalWrapper);

    rpaModalDotsContainer.appendChild(rpaModalDotSpan);
  });

  rpaModalContainer.appendChild(rpaModalDotsContainer);

  document.body.style.position = "relative";
  document.body.appendChild(rpaModalContainer);

  let index = 0;
  let slides = document.querySelectorAll(".rpa-modal-bottom-left .rpa-modal-slides-wrapper");
  let dot = document.querySelectorAll(".rpa-modal-bottom-left .rpa-modal-image-dot");

  function changeSlide() {
    if (index < 0) {
      index = slides.length - 1;
    }

    if (index > slides.length - 1) {
      index = 0;
    }

    for (let i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
      dot[i].classList.remove("rpa-modal-image-dot-active");
    }

    slides[index].style.display = "block";
    dot[index].classList.add("rpa-modal-image-dot-active");

    index++;

    setTimeout(changeSlide, 5000);
  }

  changeSlide();
}

//----------單選型模組、無按鈕模組----------
function modalVerOneClose(event) {
  document.querySelectorAll(".rpa_show_type_3_4_main").forEach((item) => {
    item.remove();
  });
}

function show_type_3_4(data, tag, scenario_ids) {
  //判斷版型
  let template_index = -1;
  let btn1_click = "modalVerOneClose(this);";
  let btn1_trace = `trace_click('${data.btn_imgId}',1,'${tag}');`;
  let btn2_click = "modalVerOneClose(this);";
  let btn2_trace = `trace_click('${data.btn2_imgId}',1,'${tag}');`;
  let btn3_click = "modalVerOneClose(this);";
  let btn3_trace = `trace_click('${data.btn3_imgId}',1,'${tag}');`;
  let divclass = "";
  let imgIds = [];

  if (data.is_background_img == "N") divclass = "nobackground-image";

  if (data.imgId != null && data.imgId != "") imgIds.push(data.imgId);
  if (data.btn_imgId != null && data.btn_imgId != "") imgIds.push(data.btn_imgId);
  if (data.btn2_imgId != null && data.btn2_imgId != "") imgIds.push(data.btn2_imgId);
  if (data.btn3_imgId != null && data.btn3_imgId != "") imgIds.push(data.btn3_imgId);

  if (data.img_url != null && data.img_url != undefined) template_index = 0;

  if (data.btn_text != null && data.btn_text != undefined) {
    template_index = 1;
    if (data.btn_url != null && data.btn_url != "") {
      btn1_click = `window.open('${data.btn_url}');`;
      btn1_trace = `trace_click('${data.btn_imgId}',1,'${tag}');`;
    }
  }

  if (data.btn2_text != null && data.btn2_text != undefined) {
    template_index = 2;
    if (data.btn2_url != null && data.btn2_url != "") {
      btn2_click = `window.open('${data.btn2_url}');`;
      btn2_trace = `trace_click('${data.btn2_imgId}',1,'${tag}');`;
    }
  }

  if (data.btn3_text != null && data.btn3_text != undefined) {
    template_index = 3;
    if (data.btn3_url != null && data.btn3_url != "") {
      btn3_click = `window.open('${data.btn3_url}');`;
      btn3_trace = `trace_click('${data.btn3_imgId}',1,'${tag}');`;
    }
  }
  const template_htmlstr = [
    `<img src="${data.img_url}" alt="${data.imgAlt}" style="max-width: 80%;max-height: 70%;" class="rpa_img" onclick="window.open('${data.target_url}');trace_click('${data.imgId}',1,'${tag}');" />`,
    `${data.html}
            <div class="rpa_btn">
                <div class="confirm_btn" onclick="${btn1_click}${btn1_trace}modalVerOneClose(this);" id="allow" style="background-color:${data.btn_color};color:${data.btn_text_color};">${data.btn_text}</div>
            </div>`,
    `${data.html}
            <div class="rpa_btn two">
                <div class="confirm_btn" onclick="${btn1_click}${btn1_trace}modalVerOneClose(this);" id="allow" style="background-color:${data.btn_color};color:${data.btn_text_color};">${data.btn_text}</div>
                <div class="confirm_btn" onclick="${btn2_click}${btn2_trace}modalVerOneClose(this);" id="forbidden" style="background-color:${data.btn_color};color:${data.btn_text_color};">${data.btn2_text}</div>
            </div>`,
    `${data.html}
            <div class="rpa_btn three">
                <a href="javascript:${btn1_click}${btn1_trace}modalVerOneClose(this);" class="confirm_btn" style="background-color:${data.btn_color};color:${data.btn_text_color};">${data.btn_text}</a>
                <a href="javascript:${btn2_click}${btn2_trace}modalVerOneClose(this);" class="confirm_btn" style="background-color:${data.btn_color};color:${data.btn_text_color};">${data.btn2_text}</a>
                <a href="javascript:${btn3_click}${btn3_trace}modalVerOneClose(this);" class="confirm_btn" style="background-color:${data.btn_color};color:${data.btn_text_color};"> ${data.btn3_text}</a>
            </div>`,
  ];

  const modalDiv = `
    <div class="rpa-lightBox_background rpa_light rpa_show_type_3_4_main" style="display: flex" >
        <div class="rpa-lightBox" style="background-color: ${data.background_color};border-color:${data.background_color}">
            <div class="rpa-closeBtn" onclick="modalVerOneClose(this);trace_click('${scenario_ids}',-1,'${tag}');">
                <img src='data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyNS4zLjEsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iX3hFNTcxX+mNo194NUZfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4Ig0KCSB5PSIwcHgiIHZpZXdCb3g9IjAgMCAzMCAzMCIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMzAgMzA7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+DQoJLnN0MHtmaWxsOiNGRkZGRkY7fQ0KPC9zdHlsZT4NCjxnPg0KCTxwYXRoIGNsYXNzPSJzdDAiIGQ9Ik0xNSwyOS43NEM2Ljg3LDI5Ljc0LDAuMjYsMjMuMTMsMC4yNiwxNVM2Ljg3LDAuMjYsMTUsMC4yNmM4LjEzLDAsMTQuNzQsNi42MSwxNC43NCwxNC43NA0KCQlTMjMuMTMsMjkuNzQsMTUsMjkuNzR6IE0xNSwyLjY5QzguMjIsMi42OSwyLjY5LDguMjIsMi42OSwxNWMwLDYuNzksNS41MiwxMi4zMSwxMi4zMSwxMi4zMXMxMi4zLTUuNTIsMTIuMy0xMi4zMQ0KCQlDMjcuMzEsOC4yMiwyMS43OSwyLjY5LDE1LDIuNjl6Ii8+DQoJPGc+DQoJCTxwYXRoIGNsYXNzPSJzdDAiIGQ9Ik0yMC42MiwyMS44NGMtMC4zMSwwLTAuNjItMC4xMi0wLjg2LTAuMzZMOC41MiwxMC4yNGMtMC40Ny0wLjQ3LTAuNDctMS4yNCwwLTEuNzINCgkJCWMwLjQ4LTAuNDcsMS4yNC0wLjQ3LDEuNzIsMGwxMS4yNCwxMS4yNGMwLjQ3LDAuNDcsMC40NywxLjI0LDAsMS43MkMyMS4yNCwyMS43MiwyMC45MywyMS44NCwyMC42MiwyMS44NHoiLz4NCgkJPHBhdGggY2xhc3M9InN0MCIgZD0iTTkuMzgsMjEuODRjLTAuMzEsMC0wLjYyLTAuMTItMC44Ni0wLjM2Yy0wLjQ3LTAuNDctMC40Ny0xLjI0LDAtMS43MkwxOS43Niw4LjUyYzAuNDctMC40NywxLjI0LTAuNDcsMS43MiwwDQoJCQljMC40NywwLjQ3LDAuNDcsMS4yNCwwLDEuNzJMMTAuMjQsMjEuNDhDMTAsMjEuNzIsOS42OSwyMS44NCw5LjM4LDIxLjg0eiIvPg0KCTwvZz4NCjwvZz4NCjwvc3ZnPg0K'/>
            </div>
            <div class="rpa-light_box_content rpa-content_background ${divclass}">
                <div class="rpa-white_background"></div>
                ${template_htmlstr[template_index]}
            </div>
        </div>
    </div>`;

  document.body.insertAdjacentHTML("beforeend", modalDiv);
}

function webpush_check_broadcast(tag) {
  let localStorageFP = window.localStorage.getItem("rpa_vid") || "";
  localStorageFP = localStorageFP.replaceAll('"', "");
  var url = `${window.atob(RPA_URL_PUSH)}/api/check_broadcast/${domain_handler()}fpid=${localStorageFP}`;

  if (tag != null && tag != undefined) {
    var type = "?";
    if (url.indexOf("?") > -1) type = "&";

    url = `${url}${type}tag=${tag}`;
    url = url.replace(/#/g, "");
  }

  var data = httpGet(url);
  if (data != "" && data != "No rpa_id") {
    var dataobj = JSON.parse(data);
    //圖片輪播-右邊
    var ROUND_IMG = [];
    if (checkIsMobile()) ROUND_IMG = dataobj.MOBILE_ROUND_IMG;
    else ROUND_IMG = dataobj.RIGHT_ROUND_IMG;

    var rtempary = [];
    if (ROUND_IMG != undefined && ROUND_IMG != null) {
      if (ROUND_IMG.data != undefined && ROUND_IMG.data != null) {
        for (var i = 0; i < ROUND_IMG.data.length; i++) {
          rtempary = rtempary.concat(ROUND_IMG.data[i]);
        }
        if (rtempary.length > 0) rpaModalRightInit(rtempary, tag, ROUND_IMG.scenario_ids);
      }
    }

    //圖片輪播-左邊
    if (!checkIsMobile()) {
      var ltempary = [];
      if (dataobj.LEFT_ROUND_IMG != undefined && dataobj.LEFT_ROUND_IMG != null) {
        if (dataobj.LEFT_ROUND_IMG.data != undefined && dataobj.LEFT_ROUND_IMG.data != null) {
          for (var i = 0; i < dataobj.LEFT_ROUND_IMG.data.length; i++) {
            ltempary = ltempary.concat(dataobj.LEFT_ROUND_IMG.data[i]);
          }
          if (ltempary.length > 0) rpaModalLeftInit(ltempary, tag, dataobj.LEFT_ROUND_IMG.scenario_ids);
        }
      }
    }

    //經典
    if (dataobj.ONE_TIME_CLASSIC != undefined && dataobj.ONE_TIME_CLASSIC != null) {
      if (dataobj.ONE_TIME_CLASSIC.data != undefined && dataobj.ONE_TIME_CLASSIC.data != null && dataobj.ONE_TIME_CLASSIC.data != []) {
        webpush_openModal(dataobj.ONE_TIME_CLASSIC.data, dataobj.ONE_TIME_CLASSIC.position, tag, dataobj.ONE_TIME_CLASSIC.scenario_ids);
      }
    }

    //單選型模組、無按鈕模組
    if (dataobj.SINGLE_TEMPLATE != undefined && dataobj.SINGLE_TEMPLATE != null) {
      if (dataobj.SINGLE_TEMPLATE.data != undefined && dataobj.SINGLE_TEMPLATE.data != null) {
        show_type_3_4(dataobj.SINGLE_TEMPLATE.data, tag, dataobj.SINGLE_TEMPLATE.scenario_ids);
      }
    }
  }
}

function trace_click(imgIds, status, tag) {
  let localStorageFP = window.localStorage.getItem("rpa_vid") || "";
  localStorageFP = localStorageFP.replaceAll('"', "");
  if (imgIds != "" && imgIds != null && imgIds != undefined && imgIds != "undefined") {
    httpGet(`${window.atob(RPA_URL_PUSH)}/api/trace_click/${domain_handler()}imgid=${imgIds}&imgstatus=${status}&tag=${tag}&fpid=${localStorageFP}&referral=${document.referrer}`);
  }
}

function httpGet(theUrl) {
  try {
    // Checking User Browser and OS System.
    let { OSName, browserType } = checkBrowserType();
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", theUrl, false); // false for synchronous request

    // If user using iPhone Device then get RPA from Local Storage.
    if (OSName === "MacOS" && browserType === "iPhone browser") {
      let localStorageRPAID = window.localStorage.getItem("rpa_id") || "";
      localStorageRPAID = localStorageRPAID.replaceAll('"', "");
      xmlHttp.setRequestHeader("MOBILE_COOKIE", `${localStorageRPAID}`);
    }

    xmlHttp.withCredentials = true;

    try {
      xmlHttp.send(null);
    } catch (error) {
      console.log({ error });
    }

    xmlHttp.addEventListener("error", (event) => {
      console.log("ERROR");
    });

    return xmlHttp.responseText;
  } catch (e) {
    console.error(e);
    return "Error HTTP Get Request";
  }
}

webpush_modalOnInit();
