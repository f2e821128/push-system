## RPA Web Push Functional

### 專案目的：

- RPA 站群推播的前端小元件
- 類似廣告的推播
- 圖片輪播
- 標準 Modal
- 客制化 Modal

### Test online at dev env. RPA_Code: A999999

[Demo Link](https://dev-ui-frontend-eyldrcphzq-an.a.run.app/RPA-webpush)

### Test Function Local

[Demo Link](https://dev-ui-frontend-eyldrcphzq-an.a.run.app/RPA-webpush/local.test.html)
