const RPA_URL = "aHR0cHM6Ly9mcC45OWxpdmUuYXNpYQ==";

function domain_handler() {
  return window.location.href.replace(/#/g, "") + (window.location.href.indexOf("?") > -1 ? "&" : "?");
}

// append script inside head tag.
function appendScriptInsideHead(url) {
  let script = document.createElement("script");
  script.src = url;
  script.type = "text/javascript";
  script.setAttribute("defer", true);
  (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script);
}

// append link inside head tag.
function appendLinkInsideHead(url) {
  let link = document.createElement("link");
  link.rel = "stylesheet";
  link.href = url;
  (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(link);
}

function getRPACode() {
  return document.querySelector("script[rpacode]").getAttribute("rpacode");
}

function httpGets(theUrl, withCookies = false) {
  try {
    let xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", theUrl, false); // false for synchronous request
    if (withCookies) {
      let localStorageRPAID = window.localStorage.getItem("rpa_id") || "";
      localStorageRPAID = localStorageRPAID.replaceAll('"', "");
      xmlHttp.setRequestHeader("MOBILE_COOKIE", `${localStorageRPAID}`);
    }
    xmlHttp.withCredentials = true;
    try {
      xmlHttp.send(null);
    } catch (error) {
      console.log({ error });
    }

    xmlHttp.addEventListener("error", (event) => {
      console.log("ERROR");
    });

    return xmlHttp.responseText;
  } catch (e) {
    console.error(e);
    return "Error HTTP Get Request";
  }
}

function httpPosts(theUrl, bodyData, withCookies = false) {
  try {
    let xmlHttp = new XMLHttpRequest();
    xmlHttp.open("POST", theUrl, false); // false for synchronous request
    xmlHttp.setRequestHeader("Content-Type", "application/json");
    if (withCookies) {
      let localStorageRPAID = window.localStorage.getItem("rpa_id") || "";
      localStorageRPAID = localStorageRPAID.replaceAll('"', "");
      xmlHttp.setRequestHeader("MOBILE_COOKIE", `${localStorageRPAID}`);
    }
    xmlHttp.withCredentials = true;
    try {
      xmlHttp.send(JSON.stringify(bodyData));
    } catch (error) {
      console.log({ error });
    }

    xmlHttp.addEventListener("error", (event) => {
      console.log("ERROR");
    });

    return xmlHttp.responseText;
  } catch (e) {
    console.error(e);
    return "Error HTTP Get Request";
  }
}

// This function is checking Browser Type & OS name
function checkBrowserType() {
  let sUsrAg = navigator.userAgent;
  let OSName = "Unknown OS";
  let browserType = "not iPhone browser";
  if (navigator.userAgent.indexOf("Mac") != -1) OSName = "MacOS";
  if (sUsrAg.indexOf("iPhone") != -1) browserType = "iPhone browser";

  return { OSName, browserType };
}

// 這裡是新增 script 在 head 上。
function getScript(source) {
  // inside header
  var script = document.createElement("script");
  script.src = source;
  script.setAttribute("name", "rpa_webpush");
  document.head.appendChild(script);
}

async function checkBroadcast() {
  let RPAKEY = getRPACode();
  let { OSName, browserType } = checkBrowserType();
  let localStorageFP = window.localStorage.getItem("rpa_vid") || "";
  localStorageFP = localStorageFP.replaceAll('"', "");

  let componentDetails = window.localStorage.getItem("bi_rpa_token") || "";

  // const scriptURL = `${window.atob(RPA_URL)}/api/check-cookie/trace/${domain_handler()}tag=${RPAKEY}&fpid=${localStorageFP}&referral=${document.referrer}`;

  if (OSName === "MacOS" && browserType === "iPhone browser") {
    let localStorageRPAID = window.localStorage.getItem("rpa_id") || "";
    localStorageRPAID = localStorageRPAID.replaceAll('"', "");
    // let mobileScript = `${window.atob(RPA_URL)}/api/mobile-check-cookie/trace/${domain_handler()}rpaid=${localStorageRPAID.toString()}&tag=${RPAKEY}&fpid=${localStorageFP}&referral=${
    //   document.referrer
    // }`;
    // httpGets(mobileScript);
    httpPosts(`${window.atob(RPA_URL)}/api/mobile-check-cookie/trace/`, {
      url: `${domain_handler()}`,
      rpaid: `${localStorageRPAID.toString()}`,
      tag: `${RPAKEY}`,
      fpid: `${localStorageFP}`,
      referral: `${document.referrer}`,
      jwt: componentDetails,
    });
  } else {
    // httpGets(scriptURL);
    httpPosts(`${window.atob(RPA_URL)}/api/check-cookie/trace/`, { url: `${domain_handler()}`, tag: `${RPAKEY}`, fpid: `${localStorageFP}`, referral: `${document.referrer}`, jwt: componentDetails });
  }

  setTimeout(function () {
    webpush_check_broadcast(RPAKEY);
  }, 2000);
}

function userAdapter(userid, type) {
  let RPAKEY = getRPACode();
  let { OSName, browserType } = checkBrowserType();
  let localStorageFP = window.localStorage.getItem("rpa_vid") || "";
  localStorageFP = localStorageFP.replaceAll('"', "");
  let urlWithFPID = "";

  if (type == "site") {
    urlWithFPID = `${window.atob(RPA_URL)}/api/send_site_acct/${userid}/${RPAKEY}?fpid=${localStorageFP}`;
  } else {
    urlWithFPID = `${window.atob(RPA_URL)}/api/send_user_info/${userid}/${RPAKEY}?fpid=${localStorageFP}`;
  }

  if (OSName === "MacOS" && browserType === "iPhone browser") {
    httpGets(urlWithFPID, true);
  } else {
    getScript(urlWithFPID);
  }
}

function sendUserInfo(userid) {
  userAdapter(userid, "normal");
}

function sendSiteUserInfo(userid) {
  userAdapter(userid, "site");
}

function cookiesOnInit() {
  // Checking User Browser and OS System.
  let { OSName, browserType } = checkBrowserType();
  if (OSName === "MacOS" && browserType === "iPhone browser") {
    let localStorageRPAID = window.localStorage.getItem("rpa_id") || "";
    // if local storage is empty then go to get new RPA_ID.
    if (localStorageRPAID === "") {
      let responseText = httpGets(`${window.atob(RPA_URL)}/api/MobileCheckRPAID/`);

      window.localStorage.setItem("rpa_id", `${responseText}`);
    }
  }
}

// Every time checking
cookiesOnInit();

appendLinkInsideHead(`${window.atob(RPA_URL)}/static/rpa_push.css`);
appendScriptInsideHead(`${window.atob(RPA_URL)}/static/fp.js`);
appendScriptInsideHead(`${window.atob(RPA_URL)}/static/rpa-fp.js`);
appendScriptInsideHead(`${window.atob(RPA_URL)}/static/push.dev.js`);

document.addEventListener("readystatechange", (event) => {
  appendScriptInsideHead(`${window.atob(RPA_URL)}/static/rpa-fp.js`);
  // appendScriptInsideHead(`./rpa-fp.js`);

  // When window loaded ( external resources are loaded too- `css`,`src`, etc...)
  if (event.target.readyState === "complete") {
    setTimeout(() => {
      const localStorageNFP = window.localStorage.getItem("bi_rpa_vid") || "";
      const localStorageFP = window.localStorage.getItem("rpa_vid") || "";
      httpGets(`${window.atob(RPA_URL)}/api/collect_mapping_data?fpid=${localStorageFP}&fpid_n=${localStorageNFP}`);
    }, 2000);
  }
});
// rpa.prd.url 202309111620
